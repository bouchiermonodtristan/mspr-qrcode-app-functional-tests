To initialize :

Autoplay :
1) Connect your mobile device with usb on your computer, in developper mode.
2) Launch AutoPlay - Run_Win_Autoplay / Run_Mac_Autoplay depending on your OS
3) Select your device in the list
4) Click on Android or Ios depending on the mobile device
5) Select in the opened windows the .apk or the Ios application files.
6) Click on view or on play.

Run the tests:
1) Once autoplay is launched and setup open the QRCode_Test folder in your favorite java IDE.
2) Build the project with dependencies. It should compile the maven dependencies for Selenium and Serenity
3) Right click on the tests folder, and click on Run Tests.

The application should launch itself on your connected device, and run the functionals tests.
Have fun :)