package com.autoplay.qrcode.tests;

import com.autoplay.qrcode.common.BaseTestTemplate;
import org.junit.Test;

public class SceneTest extends BaseTestTemplate {

    @Test
    public void StartPage(){

        /** We start with one QRCode **/
        
        /** Try to place on QRCode (Top of the screen) **/
        player.atStartPage.clickQRCodeButton();
        player.atStartPage.assertSeleniumTextEqual("QRCodeCamera OK");
        /** Try to place on Modal (Middle of the screen) **/
        player.atStartPage.clickModalWindowTopButton();
        player.atStartPage.assertSeleniumTextEqual("Modal Window OK");
        /** Try to open the camera */
        player.atStartPage.clickQRCodeTopButton();
        player.atStartPage.assertSeleniumTextEqual("QRCode OK");
        
        /* Try to open a modale while not beeing on ModaleMenu Hovered ->
        Don't update Selenium Text*/
        player.atStartPage.clickModalOpenButton();
        player.atStartPage.assertSeleniumTextEqual("QRCode OK");
        
        /* Try to open a modale while beeing on ModaleMenu Hovered ->
        Update selenium Text*/
        player.atStartPage.clickModalWindowTopButton();
        player.atStartPage.clickModalOpenButton();
        player.atStartPage.assertSeleniumTextEqual("Style 2 - Standard(Clone) Opened");
        
        /** Try to close the popup */
        player.atStartPage.clickModalOKButton();
        player.atStartPage.assertSeleniumTextEqual("Style 2 - Standard(Clone) Closed");
        
        /** Try to generate a new QRCode */
        player.atStartPage.clickGenerateCodeButton();
        player.atStartPage.assertSeleniumTextEqual("HttpManager OK");
        
        /** Verify that we have two QRCode **/
        player.atStartPage.verifyQrCode2Exist();
        
        /** Delete the second QRCode **/
        player.atStartPage.clickModal2OpenButton();
        player.atStartPage.clickModal2DeleteButton();
        
    }
}
