package com.autoplay.qrcode.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class StartPage extends PageObject {

    @FindBy(id = "QRCodeCamera")
    private WebElementFacade QRCodeCamera;
    
    @FindBy(id = "FunctionalTestResult")
    private WebElementFacade seleniumText;

    @FindBy(id = "QRCode_index1")
    private WebElementFacade QRCodeTopButton;
    
    @FindBy(id = "Modal_Window")
    private WebElementFacade ModalWindowTopButton;
    
    @FindBy(id = "Standard_Clone_")
    private WebElementFacade ModalOpenButton;
    
    @FindBy(id = "OK")
    private WebElementFacade ModalOKButton;
    
    @FindBy(id = "SeleniumGenerateCode")
    private WebElementFacade GenerateCodeButton;
    
    @FindBy(xpath = "/root[1]/Canvas[1]/Content[1]/Scroll_Area[1]/List[1]/Modal_Windows[1]/MyCodes[1]/Standard_Clone_[2]")
    private WebElementFacade GeneratedCode;
     
    @FindBy(xpath = "/root[1]/Canvas[1]/Modal_Windows[1]/Style_2_Standard_Clone_[2]/Main_Content[1]/Buttons[1]/Content[1]/Delete[1]")
    private WebElementFacade Modal2DeleteButton;
    ////////////////////  INPUT ACTIONS SECTION START  /////////////////////////////////////////

    public void clickQRCodeButton(){
        QRCodeCamera.click();
    }

    public void clickQRCodeTopButton(){
        QRCodeTopButton.click();
    }
    
    public void clickModalWindowTopButton(){
        ModalWindowTopButton.click();
    }
    
    public void clickModalOpenButton(){
        ModalOpenButton.click();
    }
    
    public void clickModalOKButton(){
        ModalOKButton.click();
    }
    
    public void clickGenerateCodeButton(){
        GenerateCodeButton.click();
    }
    
    public void clickModal2OpenButton(){
        GeneratedCode.click();
    }
    
    public void clickModal2DeleteButton()
    {
        Modal2DeleteButton.click();
    }
    
    ////////////////////  INPUT ACTIONS SECTION END  /////////////////////////////////////////


    ////////////////////  VERIFY ACTIONS SECTION START  /////////////////////////////////////////

    public void assertSeleniumTextEqual(String msg) {
        Assert.assertEquals(msg, seleniumText.getText());
    }

    public void verifyQrCode2NotExist()
    {
        GeneratedCode.shouldNotBePresent();
    }

    public void verifyQrCode2Exist()
    {
        GeneratedCode.shouldBePresent();
    }
    ////////////////////  VERIFY ACTIONS SECTION END  /////////////////////////////////////////
}
