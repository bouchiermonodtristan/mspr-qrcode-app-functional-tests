package com.autoplay.qrcode.steps.player;

import com.autoplay.qrcode.pages.StartPage;
import com.autoplay.qrcode.steps.CommonSteps;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;

public class AtStartPage extends CommonSteps {

    StartPage startPage;

    @Step
    public void clickQRCodeButton(){
        startPage.clickQRCodeButton();
    }
    
    @Step
    public void clickQRCodeTopButton(){
        startPage.clickQRCodeTopButton();
    }
    
    @Step
    public void clickModalWindowTopButton(){
        startPage.clickModalWindowTopButton();
    }
    @Step
    public void clickModalOpenButton()
    {
        startPage.clickModalOpenButton();
    }
    @Step
    public void clickModalOKButton()
    {
        startPage.clickModalOKButton();
    }
    @Step
    public void clickGenerateCodeButton(){
        startPage.clickGenerateCodeButton();
    }
    @Step
    public void assertSeleniumTextEqual(String msg){
        startPage.assertSeleniumTextEqual(msg);
    }
    @Step
    public void verifyQrCode2NotExist()
    {
        startPage.verifyQrCode2NotExist();
    }
    @Step
    public void verifyQrCode2Exist()
    {
        startPage.verifyQrCode2Exist();
    }
    @Step
    public void clickModal2OpenButton(){
        startPage.clickModal2OpenButton();
    }
    @Step
    public void clickModal2DeleteButton()
    {
        startPage.clickModal2DeleteButton();
    }
}
