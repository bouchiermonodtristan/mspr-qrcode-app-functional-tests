package com.autoplay.qrcode.steps;

import com.autoplay.qrcode.steps.player.AtStartPage;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;

public class PlayerSteps extends ScenarioSteps {

    @Steps
    public AtStartPage atStartPage;

}
